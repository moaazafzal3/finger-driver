﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathGenerator : MonoBehaviour {
    public static PathGenerator instance;
    public LineRenderer line;
    public EdgeCollider2D edgeLeft, edgeRight;
    public List<Vector3> pointsMid,pointsLeft,pointsRight;
    public Transform marching;
    float lineWidth = 1;
    int minSteps = 25;
    int maxSteps = 100;
    float StepDist = 0.06f;
    public GameObject diamond;
    public GameObject shield;

    // Use this for initialization
    private void Awake()
    {
        instance = this;
    }
    void Start () {
        Application.targetFrameRate = 60;

        StartCoroutine(CreateWorld());
        StartCoroutine(DestroyWorld());
        StartCoroutine(DrawWorld());
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(turn());
        }
	}
    bool reachedWorldSize;
    int startingWorldSize = 300;
    float maxDistanceFromMarching = 6f;

    IEnumerator DrawWorld()
    {
        while (true)
        {
            ApplyToLineRenderer();
           
            yield return new WaitForEndOfFrame();

        }
    }
    IEnumerator DestroyWorld()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            if (pointsMid[1].y<CarMovementManager.instance.transform.position.y &&
                Vector3.Distance(CarMovementManager.instance.transform.position, pointsMid[1]) > maxDistanceFromMarching*1.5f)
            {

                pointsMid.RemoveAt(0);
                pointsLeft.RemoveAt(0);
                pointsRight.RemoveAt(0);
            }
        }
    }
    IEnumerator CreateWorld()
    {
        while (true)
        {
            int findDirection = Random.Range(0, 1 + 1);

            if (findDirection == 0) findDirection = -1;

            int degrees = 90*findDirection;

            yield return turn(degrees,Random.Range(minSteps,maxSteps),StepDist);
            yield return turn(0, Random.Range(1, maxSteps), Random.Range(StepDist/4,StepDist));
           yield return turn(-degrees, Random.Range(minSteps, maxSteps), StepDist);
            yield return turn(0, Random.Range(1, maxSteps ), Random.Range(StepDist / 4, StepDist));

        }


    }


    IEnumerator turn(float degrees = 90, int steps = 50, float distanceMod=0.1f)
    {
        Vector3 currentDegrees = marching.eulerAngles;
        Vector3 targetDegrees = currentDegrees;
        targetDegrees.z -= degrees;
        float lerper = 0;

        for (int i = steps; i >= 0; i--)
        {

            if (reachedWorldSize == true)
            {
                while (marching.position.y-CarMovementManager.instance.transform.position.y> maxDistanceFromMarching)
                {
                    yield return new WaitForEndOfFrame();
                }
            }



            lerper += (degrees / steps) / degrees;

            if (degrees == 0)
            {
                marching.position += marching.up * distanceMod*steps;
                i = -10;

            }
            else
            {
                marching.position += marching.up * distanceMod;
            }

            if (degrees != 0)
            {

                Vector3 eulers = marching.eulerAngles;
                eulers.z = Mathf.LerpAngle(currentDegrees.z, targetDegrees.z, lerper);

                marching.eulerAngles = eulers;

             
            } 

            // setting points

           
                pointsLeft.Add(marching.position - marching.right * lineWidth);
                pointsRight.Add(marching.position + marching.right * lineWidth);
                pointsMid.Add(marching.position);
            if (haveToSpawnPowerUp())
            {
                SpawnPowerUps();
            }






            if (pointsMid.Count >= startingWorldSize)
            {
                reachedWorldSize = true;
               
            }

            yield return new WaitForEndOfFrame();


        }
        yield return null;
    }

    void SpawnPowerUps()
    {
        if (pointsMid.Count < 10) return;

        GameObject powerUpToUse = diamond;

        int shieldProbability = 10;
        int shieldRandom = Random.Range(0, shieldProbability);

        if (shieldRandom == 0)
        {
            powerUpToUse = shield;
        }

        GameObject newPowerUp = (GameObject)Instantiate(powerUpToUse, Vector3.zero,Quaternion.identity);

        // get spawnPoint
        Vector3 longAxis = Vector3.Lerp(pointsMid[pointsMid.Count-1], 
        pointsMid[pointsMid.Count - 2],(float)Random.Range(0,100)/100);
        int randomDir = Random.Range(0, 1+1);
        if (randomDir == 0) randomDir = -1;
        longAxis += randomDir* (marching.transform.right * ((float)Random.Range(0, 100) / 100))*(lineWidth*0.5f);
        newPowerUp.transform.position = longAxis;

    }

    bool haveToSpawnPowerUp()
    {
        int probability = 80;
        int randomProb = Random.Range(0, probability);
        if (randomProb == 0) { return true; }
        return false;
    }

    void ApplyToLineRenderer()
    {
     
       

        line.positionCount = pointsMid.Count;
        line.SetPositions(pointsMid.ToArray());


        Vector2[] lpoint = new Vector2[pointsLeft.Count];
        Vector2[] rpoint = new Vector2[pointsRight.Count];
        lpoint = VectorExtension.toVector2(pointsLeft.ToArray());
        rpoint = VectorExtension.toVector2(pointsRight.ToArray());

        if (lpoint.Length >= 2)
        {
            edgeLeft.points = lpoint;
            edgeRight.points = rpoint;
        }
    }




}
