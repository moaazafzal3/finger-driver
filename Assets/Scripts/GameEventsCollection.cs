﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventsCollection : MonoBehaviour {

    public static GameEventsCollection instance;

    // block to enable game
    public CarMovementManager carMovement;
    public PathGenerator lineCreator;
    public ColorsManager colorsManager;
    public TargetFollow cameraTargetFollow;
    public GameObject startingLine;
    public GameObject line;
    public GameObject wheel;

    void Awake()
    {
        instance = this;
    }

    public bool GameStarted;

    public void StartGame()
    {
        GameStarted = true;

        carMovement.enabled = true;
        lineCreator.enabled = true;
        colorsManager.enabled = true;
        startingLine.SetActive(true);
        line.SetActive(true);
        if (!CarMovementManager.instance.isNotStering)
        {
            wheel.SetActive(true);
        }
        else
        {
            wheel.SetActive(false);
        }
        


}

    public void IncreasePoint(int val)
    {
         ScoreHandler.instance.increaseSpecialPoints(val);

    }

    public void IncreaseScore(int val)
    {
        ScoreHandler.instance.increaseScore(val);

    }

    bool dead;
    public void Death()
    {
        if (dead) return;

        dead = true;
        SoundsManager.instance.PlayExplosionSound();
       ObliusGameManager.instance.GameOver(1);
        wheel.SetActive(false);
    }



}
