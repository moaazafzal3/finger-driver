﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using System.Net;
using System.IO;

public class UnityAds : MonoBehaviour
{
    public string gameId ;
    public bool testMode ;
    string rewaded = "rewardedVideo";
    string banner = "banner";
    public static UnityAds instance;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        Advertisement.Initialize(gameId, testMode);

    }

    //Banner

    public void ShowBanner()
    {
        StartCoroutine(ShowBannerWhenInitialized());
    }
    IEnumerator ShowBannerWhenInitialized()
    {
        while (!Advertisement.isInitialized)
        {
            yield return new WaitForSeconds(0.5f);
        }
        Advertisement.Banner.Show(banner);
    }

    //Interstitial

    public void ShowInterstitialAd()
    {
        // Check if UnityAds ready before calling Show method:
        //if (GameManager.dataSave.noAds)
        //{
        //    Debug.Log("No Ads");
        //    return;
        //}
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
            
        }
        else
        {
            Debug.Log("Interstitial ad not ready at the moment! Please try again later!");
        }
    }
  
    // Rewarded

    public void ShowRewardedVideo()
    {

            var options = new ShowOptions { resultCallback = AdCallbackhanler };
            Advertisement.Show(rewaded, options);
        
        
    }
    void AdCallbackhanler(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                CarDeathManager.instance.AfterREwardedAds();
                //Reward Bomb
               // PopupLose.instance.AfterRewardAd();
                break;
            case ShowResult.Skipped:
                Debug.Log("Ad Skipped");
                CarDeathManager.instance.GameOverFun();
               // PopupContinue.instance.NoThank();
                break;
            case ShowResult.Failed:
                Debug.Log("Ad failed");
                CarDeathManager.instance.GameOverFun();
                //PopupContinue.instance.NoThank();
                break;
        }
    }
    
}
