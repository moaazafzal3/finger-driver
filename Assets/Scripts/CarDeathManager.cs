﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDeathManager : MonoBehaviour {


    bool isDead;
    public GameObject explosionParticle;
    public GameObject particlesCompound;
    public ParticleSystem smokeParticle;
    public ParticleSystem.EmissionModule smoke;
    public static CarDeathManager instance;
    public bool secondChance;

    private void Awake()
    {
        secondChance = false;
        instance = this;
        smoke = smokeParticle.emission;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isDead || ShieldManager.instance.isShieldTaken) return;
        if (ShieldManager.instance.isShieldActive)
        {
            ShieldManager.instance.Teleport(false);
            SoundsManager.instance.PlayShieldHitSound();
            return;
        }
        Debug.Log("End");
        CarMovementManager.instance.canMove = false;
        if (!secondChance)
        {
            Debug.Log("In");

            CameraShakeEffect.instance.ActivateEffect();
            particlesCompound.transform.parent = null;
            explosionParticle.SetActive(true);
            explosionParticle.transform.rotation = Quaternion.identity;
            smoke.enabled = false;
            isDead = true;
            SoundsManager.instance.PlayExplosionSound();
            secondChance = true;
            Invoke("SecondChanceOpen", .5f);
        }
        else
        {
            Debug.Log("Out");
            GameOverFun();
        }
        // Application.LoadLevel(Application.loadedLevel);
        
    }
    void SecondChanceOpen()
    {
        GUIManager.instance.ShowOneMoreChanceGUI();
    }
    public void AfterREwardedAds()
    {
        isDead = false;
        CarMovementManager.instance.canMove = true;
        ShieldManager.instance.Teleport(true);
        SoundsManager.instance.PlayShieldHitSound();
        GUIManager.instance.HideOneMoreChanceGUI();
    }

    public void GameOverFun()
    {
        PathGenerator.instance.StopAllCoroutines();
        GUIManager.instance.HideOneMoreChanceGUI();
        CameraShakeEffect.instance.ActivateEffect();
        particlesCompound.transform.parent = null;
        explosionParticle.SetActive(true);
        explosionParticle.transform.rotation = Quaternion.identity;
        smoke.enabled = false;
        isDead = true;
        Destroy(this.gameObject);
        GameEventsCollection.instance.Death();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("path"))
        {
            BgCreator.instance.createdBg(collision.transform);
            //collision.transform.position = collision.transform.position + new Vector3(0, 40.2f,0);
        }
    }
}
