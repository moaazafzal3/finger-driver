﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowManager : MonoBehaviour {
    Vector3 positionoffset;
	// Use this for initialization
	void Start () {
        positionoffset = transform.position - transform.parent.position ;
	}
	
	// Update is called once per frame
	void Update () {
        float ydifference =(transform.parent.rotation.z)/25f;
        transform.position = (transform.parent.position+positionoffset) + ((Vector3.up-Vector3.right).normalized * ydifference);
	}
}
