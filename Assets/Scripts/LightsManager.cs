﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightsManager : MonoBehaviour {
    public SpriteRenderer light1, light2;
	// Use this for initialization
	void Start () {
        StartCoroutine(LightAnimation());
	}

    IEnumerator LightAnimation()
    {
        float lerperTime = 1f;
        Color currentColor = light1.color;
        Color alphacolor = currentColor;alphacolor.a = 0;

        while (true)
        {
            float lerper = 0;

            while (lerper <= 1)
            {
                lerper += Time.deltaTime / lerperTime;
                light1.color = Color.Lerp(currentColor, alphacolor, lerper);
                light2.color = Color.Lerp(currentColor, alphacolor, lerper);

                yield return new WaitForEndOfFrame();
            }

            lerper = 0;

            while (lerper <= 1)
            {
                lerper += Time.deltaTime / lerperTime;
                light1.color = Color.Lerp(alphacolor, currentColor, lerper);
                light2.color = Color.Lerp(alphacolor, currentColor, lerper);

                yield return new WaitForEndOfFrame();
            }
            if (ShieldManager.instance.isShieldTaken)
            {
                yield return new WaitForEndOfFrame(); // wait until the respawn animation is expired
            }
        }
    }

}
