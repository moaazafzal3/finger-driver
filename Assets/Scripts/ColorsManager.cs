﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorsManager : MonoBehaviour {
    public LineRenderer lineNormal;
        public LineRenderer lineStart;
    public List<ColorTemplate> templateList = new List<ColorTemplate>();
    ColorTemplate currentTemplate;
    // Use this for initialization
	void Start () {
        ChooseRandomTemplate();
	}


    void ChooseRandomTemplate()
    {

        currentTemplate = templateList[Random.Range(0, templateList.Count)];

        Color currentLineColor = currentTemplate.colorLine;
        currentLineColor.a = 1; // cannot be transparent

        lineNormal.startColor = currentLineColor;
        lineNormal.endColor = currentLineColor;
        lineStart.startColor = currentLineColor;
        lineStart.endColor = currentLineColor;


        Camera.main.backgroundColor = currentTemplate.colorTemplate;
    }
}
