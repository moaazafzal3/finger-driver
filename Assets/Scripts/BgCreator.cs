﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgCreator : MonoBehaviour
{
    public List<Transform> bgList;
    public GameObject BgPrefab;
    public static BgCreator instance;
    private void Start()
    {
        instance = this;
    }

    public void createdBg(Transform currentObj)
    {
        bool check = false;
        //Check Upper Position
        for (int i = 0; i < bgList.Count; i++)
        {
            Vector3 temp = currentObj.position + new Vector3(0, 20.48f, 0);
            if (temp == bgList[i].position)
            {
                check = true;
                break;
            }
        }
        if (!check)
        {
            check = false;
            GameObject obj = GameObject.Instantiate(BgPrefab,currentObj.position+new Vector3(0,20.48f,0),currentObj.rotation);
            obj.transform.parent = transform;
            bgList.Add(obj.transform);
        }

        check = false;
        //Check Right Position
        for (int i = 0; i < bgList.Count; i++)
        {
            Vector3 temp = currentObj.position + new Vector3(20.48f, 0, 0);
            if (temp == bgList[i].position)
            {
                check = true;
                break;
            }
        }
        if (!check)
        {
            
            GameObject obj = GameObject.Instantiate(BgPrefab,( currentObj.position + new Vector3(20.48f, 0f, 0)), currentObj.rotation);
            obj.transform.parent = transform;
            bgList.Add(obj.transform);
        }

        check = false;
        //Check Left Position
        for (int i = 0; i < bgList.Count; i++)
        {
            Vector3 temp = currentObj.position - new Vector3(20.48f, 0, 0);
            if (temp == bgList[i].position)
            {
                
                check = true;
                break;
            }
        }
        if (!check)
        {
            Debug.Log("LEFT");
            check = false;
            GameObject obj = GameObject.Instantiate(BgPrefab, (currentObj.position - new Vector3(20.48f, 0f, 0)), currentObj.rotation);
            obj.transform.parent = transform;
            bgList.Add(obj.transform);
        }
    }

}
