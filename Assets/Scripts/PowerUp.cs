﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {
    public SpriteRenderer powerupSprite;
    public SpriteRenderer shadowSprite;
    float FloatDistance = 0.2f;
    bool taken;
    bool destroying;
    float destroyingTimer = 10;

    public enum PowerUpType
    { diamond, shield}
    public PowerUpType kind;

	// Use this for initialization
	void Start () {
        StartCoroutine(constantMovement());
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (taken) return;
        StopAllCoroutines();
        StartCoroutine(takeMovement());
        taken = true;
        destroying = true;

        if (kind == PowerUpType.diamond)
        {
            TakeDiamond();
        }
        if (kind == PowerUpType.shield)
        {
            TakeShield();
        }
    }

    void TakeShield()
    {
        ShieldManager.instance.TakeShield();
        SoundsManager.instance.PlayCollectShieldSound();
    }

    void TakeDiamond()
    {
        GameEventsCollection.instance.IncreasePoint(1);
        SoundsManager.instance.PlayCollectDiamondSound();

    }


    private void Update()
    {
        if (destroying)
        {
            destroyingTimer -= Time.deltaTime;
        }
        if (destroyingTimer <= 0)
        {
            Destroy(this.gameObject);
        }
        if (CarMovementManager.instance != null)
        {
            if (transform.position.y + 10 < CarMovementManager.instance.transform.position.y)
            {
                destroying = true;
            }
        }
    }


    IEnumerator takeMovement()
    {
        float lerperTime = 0.5f;

        float speed = 2;
      
            float lerper = 0;

        Color currentColor = powerupSprite.color;
        Color targetColor = powerupSprite.color; targetColor.a = 0;

        Color currentShadowCol = shadowSprite.color;
        Color targetShadowCol = shadowSprite.color; targetShadowCol.a = 0;

            while (lerper <= 3)
            {
                lerper += Time.deltaTime / lerperTime;
            speed += Time.deltaTime * 15;
            powerupSprite.transform.position += Vector3.up* speed*Time.deltaTime;
            powerupSprite.color = Color.Lerp(currentColor, targetColor, lerper);
            shadowSprite.color = Color.Lerp(currentShadowCol, targetShadowCol, lerper);

            yield return new WaitForEndOfFrame();
            }
        
    }
    IEnumerator constantMovement()
    {
        float lerperTime = 0.5f;
        Vector3 startingPoint = powerupSprite.transform.position;
        Vector3 topPoint = startingPoint; topPoint += Vector3.up* FloatDistance;


        while (true)
        {
            float lerper = 0;

            while (lerper <= 1)
            {
                lerper += Time.deltaTime / lerperTime;
                powerupSprite.transform.position = Vector3.Lerp(startingPoint, topPoint, lerper);
                yield return new WaitForEndOfFrame();
            }

            lerper = 0;
           
            while (lerper <= 1)
            {
                lerper += Time.deltaTime / lerperTime;
                powerupSprite.transform.position = Vector3.Lerp(topPoint, startingPoint, lerper);
                yield return new WaitForEndOfFrame();
            }
        }

    }
}
