﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorTemplate  {

    public Color colorLine;
   public Color colorTemplate;
	
    public ColorTemplate(Color colLine,Color colBg)
    {
        colorLine = colLine;
        colorTemplate = colBg;
    }
}
