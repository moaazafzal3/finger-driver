﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldManager : MonoBehaviour {
    public static ShieldManager instance;
    List<Vector3> positions = new List<Vector3>();
    List<Vector3> rotations = new List<Vector3>();
    public bool isShieldActive;
    public bool isShieldTaken;
    public SpriteRenderer shieldSprite;
    private void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start () {

        StartCoroutine(SavePositions());
	}

    private void Update()
    {
        if (isShieldActive)
        {
            shieldSprite.enabled = true;
        }
        else
        {
            shieldSprite.enabled = false;

        }
    }

    public void Teleport(bool flag)
    {
        if (flag)
        {
            transform.position = positions[0] - transform.up;
        }
        else
        {
            transform.position = positions[0];
        }
        
        transform.eulerAngles = rotations[0];
        isShieldActive = false;
        isShieldTaken = true;
        StartCoroutine(RespawnAnimation());
    }

    IEnumerator RespawnAnimation()
    {
        Vector3 scale = transform.localScale;
        Vector3 targetScale = scale * 3;
        float lerperTime = 1.25f;
        float lerper = 0;

        SpriteRenderer carSprite = CarGraphicsManager.instance.carSprite;
        Color currentcolor = carSprite.color;
        Color targetColor = currentcolor;
        targetColor.a = 0;

        while (lerper <= 1)
        {
            lerper += Time.deltaTime / lerperTime;
            transform.localScale = Vector3.Lerp(targetScale, scale, lerper);
            carSprite.color = Color.Lerp(targetColor, currentcolor, lerper);
            yield return new WaitForEndOfFrame();
        }
        isShieldTaken = false;
    }



    public void TakeShield()
    {
        isShieldActive = true;
    }
    IEnumerator SavePositions() {

        while (true)
        {
            yield return new WaitForSeconds(1);

            positions.Add(transform.position);
            rotations.Add(transform.eulerAngles);

            if (positions.Count > 2)
            {
                positions.RemoveAt(0);
                rotations.RemoveAt(0);
            }

        }
    }
}
