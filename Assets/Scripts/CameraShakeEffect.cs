﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeEffect : MonoBehaviour {
    public static CameraShakeEffect instance;

    private void Awake()
    {
        instance = this;
    }

    public void ActivateEffect()
    {
        StartCoroutine(EffectRoutine());
    }
    IEnumerator EffectRoutine()
    {
        float lerper = 0;
        float lerperTime = 0.3f;
        float sensivity = 0.1f;
        Vector3 originalPosition = Camera.main.transform.position;
        while (lerper <= 1)
        {
            lerper += Time.deltaTime/lerperTime;

            Camera.main.transform.position = originalPosition+ Vector3.up * Random.Range(-sensivity, sensivity);
            Camera.main.transform.position += Vector3.right * Random.Range(-sensivity, sensivity);

            yield return new WaitForEndOfFrame();
        }
    }

}
