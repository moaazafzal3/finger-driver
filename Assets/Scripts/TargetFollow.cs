﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFollow : MonoBehaviour {
    public Transform targetToFollow;
    public Vector3 offset;
	// Use this for initialization
	void Start () {
        offset = (-transform.position + targetToFollow.position);
	}
	
	// Update is called once per frame
	void Update () {
		if (targetToFollow != null)
		{
			transform.position = Vector3.Lerp(transform.position, targetToFollow.transform.position - offset, 8 * Time.deltaTime);
		}
	}
}
