﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovementManager : MonoBehaviour {
    public SteeringWheel wheel;
    public float sensivityModifier = 1;
    public float speed = 2.6f;
    public bool isNotStering;
    public float second;
    public static CarMovementManager instance;
	// Use this for initialization
	void Awake () {
        instance = this;
	}

    float previousAngle;
    float currentAngle;
    // Update is called once per frame
    float difference;
    public bool canMove = false;
    private void OnEnable()
    {
        speed = 2.6f;
        StartCoroutine(canMoveRoutine());
    }
    IEnumerator canMoveRoutine()
    {
        float lerper = 0;
        float lerperTime = 0.5f;
        Vector3 current = transform.position;
        Vector3 target = current;
        target += Vector3.up * 2;
        while (lerper <= 1)
        {
            transform.position = Vector3.Lerp(current, target, lerper);
            lerper += Time.deltaTime / lerperTime;
            yield return new WaitForEndOfFrame();
        }
        GameEventsCollection.instance.cameraTargetFollow.enabled = true;
        canMove = true;
        while (canMove)
        {
            yield return new WaitForSeconds(0.1f);
            GameEventsCollection.instance.IncreaseScore(1);
        }
    }
    void Update () {

        if (!isNotStering)
        {

            if (ShieldManager.instance.isShieldTaken == false && canMove)
            {

                currentAngle = -wheel.wheelAngle;

                currentAngle = Mathf.Clamp(currentAngle, -60, 60);

                transform.Rotate(0, 0, (currentAngle * sensivityModifier*2) * Time.deltaTime);

                previousAngle = currentAngle;

                transform.position += transform.up * speed * Time.deltaTime;
            }
        }
        else
        {
            if (ShieldManager.instance.isShieldTaken == false && canMove)
            {
                var playerScreenPoint = Camera.main.WorldToScreenPoint(Input.mousePosition);
                if (Input.GetMouseButtonDown(0))
                {
                    currentAngle = 0;
                }
                if (Input.mousePosition.x > (Screen.width / 2)  && Input.GetMouseButton(0))
                {
                    currentAngle = currentAngle-second;
                   // Debug.Log("Position : " + myX);
                    currentAngle = Mathf.Clamp(currentAngle, -60, 60);
                    Debug.Log("angel : " + currentAngle);
                    transform.Rotate(0, 0, (currentAngle * sensivityModifier * 2f) * Time.deltaTime);
                }
                else if (Input.mousePosition.x < (Screen.width / 2) && Input.GetMouseButton(0))
                {
                    currentAngle = currentAngle+ second;
                //    Debug.Log("Position2 : " + myX);
                    currentAngle = Mathf.Clamp(currentAngle, -60, 60);
                    Debug.Log("angel2 : " + currentAngle);
                    transform.Rotate(0, 0, (currentAngle * sensivityModifier * 2f) * Time.deltaTime);
                }

                

                transform.position += transform.up * speed * Time.deltaTime;
            }
        }

	}

}
