﻿            using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionArrowsEffect : MonoBehaviour {
    public SpriteRenderer[] sprites;
    float speed = 1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		foreach (SpriteRenderer sprite in sprites)
        {
            sprite.transform.position += sprite.transform.up * Time.deltaTime*speed;
            speed += Time.deltaTime * 5;
        }
	}
}
