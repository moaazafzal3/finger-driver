﻿using UnityEngine;
using System.Collections;

public class ObliusGameManager : MonoBehaviour
{

	public static ObliusGameManager instance;

	public enum GameState
	{
		menu,
		game,
		gameover,
		shop

	}

	public GameState gameState;
	public bool oneMoreChanceUsed = false;

	void Awake ()
	{
		instance = this;
	}
	// Use this for initialization
	void Start ()
	{
		Application.targetFrameRate = 60;
	}

	// Update is called once per frame
	void Update ()
	{

	}

	public IEnumerator GameOverCoroutine (float delay)
	{
		gameState = GameState.gameover;
		yield return new WaitForSeconds (delay);
		SoundsManager.instance.PlayGameOverSound ();

		//Leaderboard.instance.reportScore (ScoreHandler.instance.score);
		GUIManager.instance.ShowGameOverGUI ();
		InGameGUI.instance.gameObject.SetActive (false);
	}

	public void OnLeaderboardButtonClick()
	{
		SoundsManager.instance.PlayMenuButtonSound();

		Leaderboard.instance.showLeaderboard();
	}

	public void GameOver (float delay)
	{
		StartCoroutine (GameOverCoroutine (delay));
	}

	public void StartGame ()
	{
        GameEventsCollection.instance.StartGame();
		ResetGame ();
		ScoreHandler.instance.incrementNumberOfGames ();
		GUIManager.instance.ShowInGameGUI ();
		gameState = GameState.game;
	}

	public void ResetGame (bool resetScore = true, bool resetOneMoreChance = true)
	{
		if (resetOneMoreChance) {
			oneMoreChanceUsed = false;
		}

		if (resetScore) {
			ScoreHandler.instance.reset ();
		}
	}


}
