﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;
public class OneMoreChanceGUI : MonoBehaviour {
   
	public UnityEvent onRewardVideoSuccess;
	public UnityEvent onRewardVideoFail;
    public Image FilledImg;
    private void OnEnable()
    {
        StartCoroutine(FillingAnimtion());
    }
    IEnumerator FillingAnimtion()
    {
        float v = 0;
        while (v < 1)
        {
            yield return new WaitForSeconds(0.01f);
            float v2 =v+ 0.004f;
            
            
            FilledImg.fillAmount = Mathf.Lerp(v, v2, Time.deltaTime * 7);
            v = v2;
        }
        CarDeathManager.instance.GameOverFun();
    }
    void Activate()
    {
        gameObject.SetActive(true);
    }

    void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void OnOneMoreChanceButtonClick()
    {
        SoundsManager.instance.PlayMenuButtonSound();


        Deactivate();
    }

    public void OnGameOverButtonClick()
    {
        //SoundsManager.instance.PlayMenuButtonSound();

        //Deactivate();
        //ObliusGameManager.instance.oneMoreChanceUsed = true;
        //ObliusGameManager.instance.GameOver(0);
        UnityAds.instance.ShowRewardedVideo();
        StopCoroutine(FillingAnimtion());
    }
}
