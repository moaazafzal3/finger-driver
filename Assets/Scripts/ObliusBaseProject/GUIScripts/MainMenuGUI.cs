﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuGUI : MonoBehaviour
{

    public static MainMenuGUI instance;

    public Text highscoreText;
    public Text gamesPlayedText;
    public Image Spin, Tap;
    public Color ActiveColor, dimColor, highlightColor;
    
    string originalHighScoreText;
    string originalGamesPlayedText;
    void Awake()
    {
        instance = this;
        originalHighScoreText = highscoreText.text;
        originalGamesPlayedText = gamesPlayedText.text;
        
    }

    // Use this for initialization
    void Start()
    {
        if (PlayerPrefs.GetInt("tap") == 1)
        {
            TapClick();
        }
        else
        {
            SpinClick();
        }
    }

    // Update is called once per frame
    void Update()
    {
        highscoreText.text = originalHighScoreText + ScoreHandler.instance.highScore;
        gamesPlayedText.text = originalGamesPlayedText + ScoreHandler.instance.numberOfGames;
    }

    public void OnShopButtonClick()
    {
        SoundsManager.instance.PlayMenuButtonSound();

        gameObject.SetActive(false);
        GUIManager.instance.ShowShopGUI();
    }

    public void OnPlayButtonClick()
    {

        SoundsManager.instance.PlayMenuButtonSound();

        ObliusGameManager.instance.StartGame();
        gameObject.SetActive(false);

    }

    public void OnRateButtonClick()
    {
        SoundsManager.instance.PlayMenuButtonSound();

        RateManager.instance.rateGame();
    }

    public void SpinClick()
    {
        CarMovementManager.instance.isNotStering = false;
        Spin.color = ActiveColor;
        Spin.GetComponentInChildren<Text>().color = highlightColor;
        Tap.color = dimColor;
        Tap.GetComponentInChildren<Text>().color = Color.black;
        PlayerPrefs.SetInt("tap", 0);
    }

    public void TapClick()
    {
        CarMovementManager.instance.isNotStering = true;
        Tap.color = ActiveColor;
        Tap.GetComponentInChildren<Text>().color = highlightColor;
        Spin.color = dimColor;
        Spin.GetComponentInChildren<Text>().color = Color.black;
        PlayerPrefs.SetInt("tap", 1);
    }

}
